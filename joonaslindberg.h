namespace joonaslindberg {
template <typename T>
class tree
{
public:
    tree();

    // Need to invent "node_handle" somehow.
    // it is almost an iterator, but not as capable as it, so I would avoid naming it "iterator" for now.
    class node_handle 
    {
    public:
       //node_handle(to be decided);

        T& operator*();
        void operator++();
        bool operator==(const node_handle& other);
    private:
        //to be decided
    };

    // Define root node value.
    //node_handle set_root(const T&);

    // Get a handle to the root node.
    //node_handle root() const;

    // Add child node to the given parent
    //node_handle add_child(node_handle parent, const T& value);

    // Return something to access child nodes for a given parent
    //std::pair<iterator?, iterator?> get_children(node_handle parent);
private:
    //to be decided
};
} // namespace joonaslindberg
